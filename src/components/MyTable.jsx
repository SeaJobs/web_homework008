import React from 'react'
import {Table,Button} from 'react-bootstrap'
function MyTable(props){
    console.log(props.items)
        return (
            <div className = "my-5">
                <h3>Table Accounts</h3>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                        <th>id</th>
                        <th>UserName</th>
                        <th>Email</th>
                        <th>Gender</th>
                        </tr>
                    </thead>
                    <tbody>
                    {
                   props.items.map((item,idx)=>
                     <tr key={idx} >
                     <td>{item.id}</td>
                     <td>{item.username}</td>
                     <td>{item.Email}</td>
                     <td>{item.Gender}</td>
                     </tr>
                )
                }

                    </tbody>
                    </Table>
                    <Button variant="danger" type="submit">
                        Delete
                      </Button>
            </div>
        )
}

export default MyTable;
