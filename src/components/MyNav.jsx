import React, { Component } from 'react'
import {Navbar} from 'react-bootstrap';

export default class MyNav extends Component {
    render() {
        return (
            <div>
                <Navbar>
                <Navbar.Brand href="#home">KSHRD Student</Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse className="justify-content-end">
                    <Navbar.Text>
                    Signed in as: <a href="#login">Em Thary</a>
                    </Navbar.Text>
                </Navbar.Collapse>
                </Navbar>
                
            </div>
        )
    }
}
