
import { Col, Container,Row,Form,Button,Figure,Card} from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import MyNav from './components/MyNav';

import MyTable from './components/MyTable';
import { Component } from 'react';


class App extends Component {
  constructor (props){
    super(props)
    this.state = {
      data : [
        {id : 1,
          username : "sopheak", 
        Gender : "male",
        Email: "sopheak@gmail.com",
        Password : "12345"
      },
      {id : 2,
        username : "chantra", 
        Gender : "female",
        Email: "chantra@gmail.com",
        Password : "12345"
      },
      {id : 3,
        username : "channy", 
        Gender : "female",
        Email: "chnnay@gmail.com",
        Password : "12345"
      }
      ],
       username : ""
      
    }
  }

onAdd = event => {
  let data = {
    username : this.state.username,
    Gender : this.state.gender,
    Email : this.state.Email,
    Password : this.state.Password,

  } 
  let getData = [... this.state.data, data]
  this.setState ({
    data: getData
  })

}

  render () {
    return (
      <div className="App">
          <Container>
             <MyNav/>             
             <Row>
                <Col lg = {4}>
                <Figure>
            <Figure.Image
              className=" d-block mx-auto img-fluid w-50"
              width={171}
              height={180}
              alt="171x180"
              src="image/avata.png"
            />
            <Card.Title><h3 className="text-center">Create Account</h3></Card.Title>
            </Figure>

                <Form>
                      <Form.Group controlId="formBasicEmail">
                        <Form.Label>UserName</Form.Label>
                        <Form.Control value={this.state.username} onChange={(e) => this.setState({ username: e.target.value })} type="text"  placeholder="Enter username" />
                        <Form.Text className="text-muted">
                        </Form.Text>
                      </Form.Group>
                      <Form>
                        <h5>Gender</h5>
                        {['radio'].map((type) => (
                          <div key={`inline-${type}`} className="mb-3">
                            <Form.Check inline label="male" name="group1" type={type} id={`inline-${type}-1`} />
                            <Form.Check inline label="female" name="group1" type={type} id={`inline-${type}-2`} />
                            
                          </div>
                        ))}
                      </Form>
                      <Form.Group controlId="formBasicPassword">
                        <Form.Label onChange={(e) => this.setState({ Email: e.target.value })}>Email</Form.Label>
                        <Form.Control type="username" placeholder="username" />
                      </Form.Group>
    
                      <Form.Group controlId="formBasicPassword">
                        <Form.Label onChange={(e) => this.setState({ Password: e.target.value })}>Password</Form.Label>
                        <Form.Control type="password" placeholder="Password" />
                      </Form.Group>
                      
                      <Button variant="success" onClick = {()=> this.onAdd()}>
                        Save
                      </Button>
                  </Form>
                </Col>
                <Col lg = {8}>
                  <MyTable items = {this.state.data}/>
                </Col>
             </Row>
          </Container>
      </div>
    );
  }
}
export default App;
